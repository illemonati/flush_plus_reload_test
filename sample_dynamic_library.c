#include <math.h>

int find_area_of_circle(float r) {
    return M_PI * r * r;
}

int find_area_of_triangle(float b, float h) {
    int a = b * h * sqrtf(h);
    int w = a * a * b * a * h;
    int k = w + b + h + b + h + b + h / 5;
    return b * h * 0.5;
}
