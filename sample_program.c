#include <stdio.h>

int find_area_of_circle(float r);

int find_area_of_triangle(float b, float h);

int main() {
    int r = 10;
    int a = 10;
    int b = 10;
    while (1) {
        printf("enter 0 to find the area of a b = 10, h = 10, triangle\n");
        printf("enter 1 to find the area of a r = 10 circle\n");
        int option;
        scanf("%d", &option);
        if (option != 0 && option != 1) {
            printf("Thats not a valid option\n");
            continue;
        }
        printf("The area is %d\n", (option == 0) ? find_area_of_triangle(a, b) : find_area_of_circle(r));
    }
}
