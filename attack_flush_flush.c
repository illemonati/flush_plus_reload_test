
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <time.h>

#define circle_location 0x0000000000000e40
#define triangle_location 0x0000000000000e71

uint64_t rdtsc() {
    uint64_t a, d;
    asm volatile("mfence");
    asm volatile("rdtsc"
                 : "=a"(a), "=d"(d));
    a = (d << 32) | a;
    asm volatile("mfence");
    return a;
}

void maccess(void* p) {
    asm volatile("movq (%0), %%rax\n"
                 :
                 : "c"(p)
                 : "rax");
}

void flush(void* p) {
    asm volatile("clflush 0(%0)\n"
                 :
                 : "c"(p)
                 : "rax");
}

size_t kpause = 0;

void flush_and_flush(void* p) {
    size_t t0 = rdtsc();
    flush(p);
    size_t t1 = rdtsc();
    size_t delta_t = t1 - t0;
    // printf("%lu\n", delta_t);
    if (delta_t > 170 && delta_t < 200) {
        if (kpause > 1000) {
            printf("Cache Hit %10lu after %10lu cycles\n", delta_t, kpause);
        }
        kpause = 0;
    }
    kpause += 1;
}

int main() {
    int fd = open("./sample_dynamic_library.so", O_RDONLY);
    unsigned char* addr = (unsigned char*)mmap(0, 64 * 1024 * 1024, PROT_READ, MAP_SHARED, fd, 0);
    printf("%p | %p | %p\n", addr, triangle_location, addr + triangle_location);
    while (1) {
        flush_and_flush(addr + triangle_location);
        for (int i = 0; i < 100; ++i)
            sched_yield();
    }
}
